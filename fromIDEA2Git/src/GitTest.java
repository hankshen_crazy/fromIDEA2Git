import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

/**
 * @author HankShen
 * @create 2022/1/21 13:29
 */
public class GitTest {
    public static void main(String[] args) {
        test3();
    }


    public static void test3() {
        //stream
        Integer reduce = Stream.of(1, 2, 3)
                .reduce(4, (acc, item) -> acc + item);
        System.out.println(reduce);

        //parallelStream
        Integer reduce1 = Stream.of(1, 2, 3).parallel()
                .reduce(4, (acc, item) -> acc + item, (finalacc, finalitem) -> finalacc + finalitem);
        System.out.println(reduce1);
    }


    /**
     * T reduce(T identity, BinaryOperator<T> accumulator);
     * 第一个参数是初始值,
     * 第二个参数是函数式接口, 即传入两个参数, 返回一个值
     *      第1个参数存放的是每一个计算的结果, 第一次存放的是前面的一个参数"初始值"
     *      第2个参数存放的是流中的一个元素
     */
    public static void test2() {
        List<Integer> list = Arrays.asList(10, -1, 30, 2);
        Integer reduce = list.stream()
                .reduce(100, (acc, item) -> acc + item);
        System.out.println(reduce);
    }


    public static void test1() {
        List<Integer> list = Arrays.asList(10, -1, 30, 2);
        Stream<Integer> stream = list.stream().sorted()
                .distinct();
        /**
         * 第一次, acc, item分别获得流中第一个和第二个数, 经过方法体中计算后将结果保存到到第一个参数中,
         * 第二次及以后, 第一个参数保存的是上一次计算的结果, 第二个参数中是流中的下一个元素
         */
        Optional<Integer> reduce = stream.reduce((acc, item) -> acc + item);
        if (reduce.isPresent()) System.out.println(reduce.get());
    }
}
